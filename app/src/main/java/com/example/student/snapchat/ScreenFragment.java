package com.example.student.snapchat;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;


public class ScreenFragment extends Fragment {


    public ScreenFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R .layout.fragment_screen, container, false);
        String[] ScreenList = {"Friends", "Camera", "Chat"};
        // Inflate the layout for this fragment

        ListView listView = (ListView) view.findViewById(R.id.screenMenu);

        ArrayAdapter<String> listViewAdapter = new ArrayAdapter<String>(
                getActivity(),
                android.R.layout.simple_list_item_1,
                ScreenList
                );
        Button logOut = (Button) view.findViewById(R.id.logOut);
        logOut.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Backendless.UserService.logout(new AsyncCallback<Void>() {
                    @Override
                    public void handleResponse(Void response) {
                        Toast.makeText(getActivity(), " You Died" , Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getActivity(), MainActivity.class);
                        startActivity(intent);
                    }

                    @Override
                    public void handleFault(BackendlessFault fault) {
                        Toast.makeText(getActivity(), " You lived" , Toast.LENGTH_SHORT).show();

                    }
                });
            }
        });
        listView.setAdapter(listViewAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    Intent intent = new Intent(getActivity(), FriendsList.class);
                    startActivity(intent);
                    Toast.makeText(getActivity(), "your cancer", Toast.LENGTH_SHORT).show();
                } else if (position == 1) {
                    Intent intent = new Intent(getActivity(), CameraActivity.class);
                    startActivity(intent);
                    Toast.makeText(getActivity(), "Kill your cancer", Toast.LENGTH_SHORT).show();
                } else if (position == 2) {
                    Intent intent = new Intent(getActivity(), ChatActivity.class);
                    startActivity(intent);
                    Toast.makeText(getActivity(), "Your a toast", Toast.LENGTH_SHORT).show();
                }
            }
        });
        return view;
    }

}
