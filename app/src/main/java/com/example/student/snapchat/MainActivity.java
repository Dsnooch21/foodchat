package com.example.student.snapchat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.backendless.Backendless;

public class MainActivity extends AppCompatActivity {
    public static final String APP_ID = "429DE0EE-B7F1-86CD-FF57-CE3292A25800";
    public static final String SECRET_KEY = "B69DF3C7-A1A1-00E0-FFEF-A0B559365A00";
    public static final String VERSION = "v1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Backendless.initApp(this, APP_ID, SECRET_KEY, VERSION);
        if (Backendless.UserService.loggedInUser() == ""){
            MainMenuFragment mainMenu = new MainMenuFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.container, mainMenu).commit();

        }else{
            ScreenFragment screenMenu = new ScreenFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.container, screenMenu).commit();

        }
    }


}
